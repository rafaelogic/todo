<?php

use App\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/logs', [TodoController::class, 'logs'])->name('logs');

Route::prefix('/todos')->group(function () {
    Route::get('/', [TodoController::class, 'fetch'])->name('todos');
    Route::post('/', [TodoController::class, 'store'])->name('todo.store');
    Route::put('/{todo}', [TodoController::class, 'update'])->name('todo.update');
    Route::delete('/{todo}', [TodoController::class, 'delete'])->name('todo.delete');
    Route::put('/{todo}/complete', [TodoController::class, 'complete'])->name('todo.complete');
});