module.exports = {
  theme: {
    colors:{
        'transparent': 'transparent',
    
        'black': '#141236',
    
        'gray-dark': '#727477',
        'gray-dark-100': '#646668',
        'gray-dark-200': '#565759',
        'gray-dark-300': '#47484A',
        'gray-dark-400': '#393A3B',
        'gray-dark-500': '#2B2B2D',
        'gray-dark-600': '#1C1D1E',
    
    
        'gray': '#BEC5C4',
        'gray-100': '#A6ACAC',
        'gray-200': '#8F9493',
        'gray-300': '#777B7B',
        'gray-400': '#474A4A',
        'gray-500': '#303131',
    
        'gray-light': '#f0f0f0',
        'gray-lighter': '#F8F9F9',
        'gray-lightest': '#FCFDFD',
    
        'white': '#ffffff',
    
        'red': '#CE283D',
        'yellow': '#F5C623',
        'green': '#31C09A',
        'light-red': '#e7919c',
        'light-green': '#7fdbc7',
    
        'blue': '#5A5CE9',
        'blue-dark': '#06205C',
        'blue-gray': '#6B7C93',
    
        'red-error': '#D8000C',
    },
    extend: {}
  },
  variants: {},
  plugins: []
}
