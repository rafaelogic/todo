require('./bootstrap');

window.Vue = require('vue');

import store from './store';

Vue.component('todo-component', require('./components/Todo').default);

new Vue({
    el: '#app',
    store,
});