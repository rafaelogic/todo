export const state = () => ({
    error: [],
    logs : [],
    todos: [],
});

export const getters = {
    error   : state => state.error,
    logs    : state => state.logs,
    todos   : state => state.todos,
};


export const mutations = {
    ERROR(state, payload) {
        state.error = payload;
    },

    LOGS(state, payload) {
        state.logs = payload;
    },

    REMOVE_TODO(state, payload) {
        // Remove the deleted todo object
        let todos = state.todos.filter(todo => todo.id !== payload.id);
        state.todos = todos;
    },

    TODOS(state, payload) {
        state.todos = payload;
    },

    UPDATE_TODOS(state, payload) {
        // Remove the outdated todo object
        let todos = state.todos.filter(todo => todo.id !== payload.id);
        
        /**
         * Checks if the state of todos length changes, if it does not it will put at 
         * the top of the list as it assumes that the user added a new todo otherwise 
         * it will put the todo at the bottom as it assumes the user mark it as complete.
         */
        (todos.length === state.todos.length) ? todos.unshift(payload) : todos.push(payload);
        
        state.todos = todos;
    },
};


export const actions = {
    complete({ commit }, todoId) {
        axios.put(`/api/todos/${todoId}/complete`).then(res => commit('UPDATE_TODOS', res.data.todo));
    },

    delete({ commit }, todoId) {
        axios.delete(`/api/todos/${todoId}`).then(res => commit('REMOVE_TODO', res.data.todo));
    },

    fetch({ commit }) {
        axios.get('/api/todos').then(res => commit('TODOS', res.data.todos));
    },

    fetchLogs({ commit }) {
        axios.get('/api/logs').then(res => commit('LOGS', res.data.logs));
    },

    store({ commit }, todo) {
        axios.post('/api/todos', todo)
            .then(res => commit('UPDATE_TODOS', res.data.todo))
            .catch(err => commit('ERROR', err.response.data.errors.title.toString()));
    },

    update( { commit }, todo) {
        axios.put(`/api/todos/${todo.id}`, todo.new)
            .then(res => commit('UPDATE_TODOS', res.data.todo))
            .catch(err => commit('ERROR', err.response.data.errors.title.toString()));
    }
};