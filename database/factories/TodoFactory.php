<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Todo;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Todo::class, function (Faker $faker) {
    $title = $faker->unique()->sentence();

    return [
        'title'         => $title,
        'slug'          => Str::slug($title, '-'),
        'is_completed'  => false,
    ];
});
