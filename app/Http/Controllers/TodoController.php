<?php

namespace App\Http\Controllers;

use App\Actions\Todo\CreateTodo;
use App\Actions\Todo\DeleteTodo;
use App\Actions\Todo\UpdateTodo;
use App\Http\Requests\StoreTodo;
use App\Models\Todo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Spatie\Activitylog\Models\Activity;

class TodoController extends Controller
{
    /**
     * Show the homepage.
     *
     * @return View
     */
    public function index() : View
    {
        return view('home');
    }

    /**
     * Get all todos.
     *
     * @return JsonResponse
     */
    public function fetch() : JsonResponse
    {
        $todos = Todo::orderBy('completed')->get();

        return response()->json([
            'todos' => $todos
        ], Response::HTTP_OK);
    }

    /**
     * Create a new todo.
     *
     * @param StoreTodo $request
     * @return JsonResponse
     */
    public function store(StoreTodo $request) : JsonResponse
    {
        $todo = app(CreateTodo::class)->execute($request);

        if ($todo->id) {
            return response()->json([
                'todo' => $todo
            ], Response::HTTP_CREATED);
        }

        return response()->json([
            'message' => 'Unable to save your todo. Please try again.'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update todo's data.
     *
     * @param Todo $todo
     * @return JsonResponse
     */
    public function update(Todo $todo, StoreTodo $request) : JsonResponse
    {
        if ($todo = app(UpdateTodo::class)->execute($todo, $request)) {
            return response()->json([
                'todo' => $todo
            ], Response::HTTP_OK);
        }
        
        return response()->json([
            'message' => 'Unable to update your todo. Please try again.'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Delete a todo.
     *
     * @param Todo $todo
     * @return JsonResponse
     */
    public function delete(Todo $todo) : JsonResponse
    {
        return response()->json([
            'todo' => app(DeleteTodo::class)->execute($todo)
        ], Response::HTTP_ACCEPTED); 
    }

    /**
     * Mark todo as complete.
     *
     * @param Todo $todo
     * @return JsonResponse
     */
    public function complete(Todo $todo) : JsonResponse
    {
        $todo->complete();

        return response()->json([
            'todo' => $todo
        ], Response::HTTP_ACCEPTED);
    }

    public function logs()
    {
        return response()->json([
            'logs' => Activity::all()
        ], Response::HTTP_OK);
    }
}
