<?php 

namespace App\Actions\Todo;

use App\Models\Todo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateTodo
{
    public function execute($request) : Todo
    {
        $title = $request->title;

        DB::beginTransaction();
        
        try {
            $todo = Todo::create([
                'title'  => $title,
                'slug'   => Str::slug($title, '-')
            ]);

            DB::commit();

            return $todo;
        } catch (\Throwable $th) {
            DB::rollBack();
            return new Todo;
        }

        if (! $todo) DB::rollBack();
    }
} 