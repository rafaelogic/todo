<?php 

namespace App\Actions\Todo;

use App\Models\Todo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UpdateTodo
{
    public function execute(Todo $todo, $request) : Todo
    {
        $title = $request->title;

        DB::beginTransaction();

        try {
            $todo->update([
                'title'  => $title,
                'slug'   => Str::slug($title, '-')
            ]);

            DB::commit();
            
            return $todo;

        } catch (\Throwable $th) {
            DB::rollBack();
            return new Todo;
        }

        if (! $todo) DB::rollBack();
    }
} 