<?php 

namespace App\Actions\Todo;

use App\Models\Todo;

class DeleteTodo
{
    public function execute(Todo $todo) : Todo
    {
        $todo->delete();
        
        return $todo;
    }
} 