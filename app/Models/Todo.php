<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Todo extends Model
{

    use LogsActivity;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The log name
     *
     * @var string
     */
    protected static $logName = 'Todo';

    /**
     * Log changes to all unguarded attributes.
     *
     * @var boolean
     */
    protected static $logUnguarded = true;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_completed'  => 'boolean',
        'completed'     => 'datetime',
    ];

    /**
     * Mark todo as completed
     *
     * @return Todo
     */
    public function complete() : Todo
    {
        $this->update([
            'is_completed'  => true,
            'completed'     => now()
        ]);

        return $this;
    }
}
