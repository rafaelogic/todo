<?php

namespace Tests\Feature;

use App\Models\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class TodoFeatureTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * App\Http\Controllers\TodoController::fetch
     */
    public function should_fetch_all_todos()
    {
        $todos = factory(Todo::class, 5)->create();

        $response = $this->withoutExceptionHandling()
                        ->getJson(route('todos'))
                        ->assertSuccessful();

        $todos->map(function ($todo) use ($response) {
            $response->assertJsonFragment([
                'id'    => $todo->id,
                'title' => $todo->title
            ]);
        });
    }

    /**
     * @test
     * App\Http\Controllers\TodoController::store
     */
    public function should_store_todo()
    {
        $data = [ 'title' => 'Sample todo' ];

        $response = $this->withoutExceptionHandling()
                        ->postJson(route('todo.store', $data))
                        ->assertStatus(Response::HTTP_CREATED);
        
        $response->assertJsonFragment($data);
    }

    /**
     * @test
     * App\Http\Controllers\TodoController::update
     */
    public function should_update_todo()
    {
        $savedTodo  = factory(Todo::class)->create();
        $editedData = [ 'title' => 'Edited todo' ];

        $response = $this->withoutExceptionHandling()
            ->putJson(route('todo.update', $savedTodo), $editedData)
            ->assertSuccessful();

        $response->assertJsonFragment($editedData);
        $this->assertEquals($savedTodo->fresh()->title, $editedData['title']);
    }

    /**
     * @test
     * App\Http\Controllers\TodoController::delete
     */
    public function should_delete_todo()
    {
        $todo  = factory(Todo::class)->create();
        
        $this->withoutExceptionHandling()
            ->delete(route('todo.delete', $todo))
            ->assertStatus(Response::HTTP_ACCEPTED);

        $this->assertEmpty(Todo::all());
    }

    /**
     * @test
     * App\Http\Controllers\TodoController::complete
     */
    public function should_mark_todo_as_complete()
    {
        $todo  = factory(Todo::class)->create();

        $this->assertNull($todo->completed);
        $this->assertFalse($todo->is_completed);

        $this->withoutExceptionHandling()
            ->put(route('todo.complete', $todo))
            ->assertSuccessful();

        $this->assertNotNull($todo->fresh()->completed);
        $this->assertTrue($todo->fresh()->is_completed);
    }
}
