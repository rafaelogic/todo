<?php

namespace Tests\Unit\Actions;

use App\Actions\Todo\UpdateTodo;
use App\Models\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateTodoTest extends TestCase
{

    use RefreshDatabase;

    /**
     * App\Actions\Todo\UpdateTodo::execute
     */
    public function test_execute()
    {
        $todo = factory(Todo::class)->create();

        $data = new \stdClass;
        $data->title = 'Edit the sample todo';

        $todo = app(UpdateTodo::class)->execute($todo, $data);

        $this->assertEquals($todo->title, $data->title);
    }
}
