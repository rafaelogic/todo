<?php

namespace Tests\Unit\Actions;

use App\Actions\Todo\DeleteTodo;
use App\Models\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteTodoTest extends TestCase
{

    use RefreshDatabase;

    /**
     * App\Actions\Todo\DeleteTodo::execute
     */
    public function test_execute()
    {
        $todo = factory(Todo::class)->create();
        $this->assertCount(1, Todo::all());
        
        app(DeleteTodo::class)->execute($todo);
        $this->assertCount(0, Todo::all());
    }
}
