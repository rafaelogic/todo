<?php

namespace Tests\Unit\Actions;

use App\Actions\Todo\CreateTodo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTodoTest extends TestCase
{

    use RefreshDatabase;

    /**
     * App\Actions\Todo\CreateTodo::execute
     */
    public function test_execute()
    {
        $data = new \stdClass;
        $data->title = 'Sample todo';

        $todo = app(CreateTodo::class)->execute($data);

        $this->assertEquals($todo->title, $data->title);
    }
}
