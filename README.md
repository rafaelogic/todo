## Note
I intended not to include JWT-Authentication for the API routes. I also made a not recommended modification in `App\Http\Middleware\VerifyCsrfToken` to exclude all routes from CSRF verification to make the app work without needing a registered user. 

## Installation
1. Run `composer install`
2. Run `npm i` or `yarn install`
3. Run `php artisan migrate`

## Dependencies
**Frontend**
- [Vue](http://vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [Laravel Mix Tailwind CSS](https://github.com/JeffreyWay/laravel-mix-tailwind)
- [Axios](https://github.com/axios/axios)

**Backend**
- [Laravel](https://laravel.com/)
- [Laravel Activity Log](https://github.com/spatie/laravel-activitylog)

## PhpUnit Testing
Run `vendor/bin/phpunit`